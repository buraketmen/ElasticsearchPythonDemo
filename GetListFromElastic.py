from elasticsearch import Elasticsearch
import json

def process_hits(mylist,hits):
    if mylist is not None:
        newlist = [item['_source'] for item in hits]
        for row in newlist:
            mylist.append(row)
        return mylist

def get_data(es,index,body):
    mylist = []
    if not es.indices.exists(index=index):
        print("Index " + index + " not exists")
        exit()

    data = es.search(
        index=index,
        scroll='100m',
        size=10000,
        body=body
    )
    sid = data['_scroll_id']
    scroll_size = len(data['hits']['hits'])
    while scroll_size > 0:
        mylist = process_hits(mylist,data['hits']['hits'])
        data = es.scroll(scroll_id=sid, scroll='100m')
        sid = data['_scroll_id']
        scroll_size = len(data['hits']['hits'])
    return mylist

es=Elasticsearch(['url'],http_auth=('user', 'password'))
index = "churnintelecoms"
body = {"query": {"match_all": {}}}
datawithmethod = get_data(es,index,body)

agg = """{"size": 0,"aggs": {"group_by_churn": {"terms": {"field": "churn","size": 2}}}}"""
group = es.search(index=index,body=agg)
listofcounts = list()
listofchurn = list()
for key in group['aggregations']['group_by_churn']['buckets']:
    listofcounts.append(key['doc_count'])
    listofchurn.append(key['key_as_string'])
print(listofcounts)
print(listofchurn)
