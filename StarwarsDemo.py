from elasticsearch import Elasticsearch
import json
import requests
import pandas as pd
import warnings
warnings.filterwarnings("ignore")
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

es=Elasticsearch(['url'],http_auth=('user', 'password'))

i = 1
while i<90:
    try:
        r = requests.get('http://swapi.co/api/people/' + str(i))
        data = json.loads(r.content)
        if 'detail' in data.keys():
            print('hatalı')
        else:
            es.index(index='starwars', id=i, body=data)
        i=i+1
    except:
        i=i+1


r = requests.get('http://swapi.co/api/people/')
j = r.json()
df = pd.DataFrame.from_dict(j['results'])
print(df.dtypes)

print(es.get(index='starwars', id=5))
res1 = es.search(index="starwars", body={"size":10000,"query": {"match_all": {}}})
print(res1)
for doc in res1['hits']['hits']:
    print(doc['_source'])
res2 = es.search(index="starwars", body={"query": {"match": {'name':'Darth'}}})
print(res2)
for doc in res2['hits']['hits']:
    print("%s) %s" % (doc['_id'], doc['_source']['height']))
    print(doc['_source']['skin_color'])

index = "starwars"
doc_type = "people"
size = 5
body = {"query": {"match_all": {}}}
# Process hits here
df = pd.DataFrame()
def process_hits(hits):
    global df
    for item in hits:
        if 'detail' in item['_source'].keys():
            pass
        else:
            df = df.append(item['_source'] , ignore_index=True)
            print(item["_source"]["name"])

# Check index exists
if not es.indices.exists(index=index):
    print("Index " + index + " not exists")
    exit()

# Init scroll by search
data = es.search(
    index=index,
    scroll='2m',
    size=size,
    body=body
)

# Get the scroll ID
sid = data['_scroll_id']
scroll_size = len(data['hits']['hits'])
while scroll_size > 0:
    "Scrolling..."
    # Before scroll, process current batch of hits
    process_hits(data['hits']['hits'])
    data = es.scroll(scroll_id=sid, scroll='2m')
    # Update the scroll ID
    sid = data['_scroll_id']
    # Get the number of results that returned in the last scroll
    scroll_size = len(data['hits']['hits'])
print(df.head(5))
df.to_csv("starwars.csv")
