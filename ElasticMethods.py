from elasticsearch import Elasticsearch
import json
import requests
import pandas as pd
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
def mapping_for_index(es,index_name, mapping):
    mapping_example = """
    {  
        "mappings":{  
            "properties":{  
                "name":    { "type": "text"},  
                "height":  { "type": "integer"}, 
                "mass":   { "type": "integer"},
                "hair_color": { "type": "text"}
            }
        }
    }"""
    es.indices.create(index=index_name,ignore=400, body=mapping)

def url_to_dataframe(url):
    r = requests.get(url)
    j = r.json()
    df = pd.DataFrame.from_dict(j['results'])
    return df

def data_to_index(es,index_name,doc_type,url,header):
    i = 1
    empty = 0
    while True:
        r = requests.get(str(url)+ str(i))
        data = json.loads(r.content)
        if 'detail' in data.keys():
            pass
        else:
            es.index(index=index_name, doc_type=doc_type, id=i, body=data)
        i=i+1

def get_item_with_id(es,index_name,doc_type,id):
    if not es.indices.exists(index=index_name):
        print("Index {} not exists".format(index_name))
    else:
        data = es.get(index=index_name, doc_type=doc_type, id=id)
        return data

def process_hits(hits):
    data_list = []
    for item in hits:
        if 'detail' in item['_source'].keys():
            pass
        else:
            data_list.append(item['_source'])
    return data_list

def get_item_with_search(es,index_name,body,size,scroll):
    if not es.indices.exists(index=index_name):
        print("Index {} not exists".format(index_name))
    else:
        data = es.search(index=index_name,scroll=scroll,size=size, body=body)
        sid = data['_scroll_id']
        scroll_size = len(data['hits']['hits'])
        while scroll_size > 0:
            process_hits(data['hits']['hits'])
            data = es.scroll(scroll_id=sid, scroll='2m')
            sid = data['_scroll_id']
            scroll_size = len(data['hits']['hits'])