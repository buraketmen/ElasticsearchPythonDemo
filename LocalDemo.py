from elasticsearch import Elasticsearch
es = Elasticsearch("http://localhost9200")
es = Elasticsearch()

es.indices.create(index="first_index", ignore=400) #index oluşturma
print(es.indices.exists(index='first_index')) #index var mı
es.indices.delete(index='first_index') #index silme
print(es.indices.exists(index='first_index'))

doc1 = {"city":"istanbul","country":"Turkey"}
doc2 = {"city":"Sakarya","country":"Turkey"}
doc3 = {"city":"Antalya","country":"Turkey"}

es.index(index="cities", doc_type="places",id=1,body=doc1)
print(es.index(index="cities", doc_type="places",id=1,body=doc1)) #eklenenleri gösterme
es.index(index="cities", doc_type="places",id=2,body=doc2)
print(es.index(index="cities", doc_type="places",id=2,body=doc2))
es.index(index="cities", doc_type="places",id=3,body=doc3)
print(es.index(index="cities", doc_type="places",id=2,body=doc3))

res = es.get(index="cities", doc_type="places", id=1)
print(res)
print(res['_source'])

doc4 = {"sentence":"Today is a sunny day."}
doc5 = {"sentence":"Today is a bright-sunny day"}
es.index(index="english", doc_type="sentences",id=1,body=doc4)
es.index(index="english", doc_type="sentences",id=2,body=doc5)

res2 = es.search(index="english", body={"from":0,"size":0,"query":{"match":{"sentence":"SUNNY"}}})
print(res2)
res3 = es.search(index="english", body={"from":0,"size":2,"query":{"match":{"sentence":"SUNNY"}}})
print(res3)
res4 = es.search(index="english", body={ "from": 0, "size": 0, "query": { "bool": { "must_not": { "match": { "sentence": "bright" } }, "should": { "match": { "sentence": "sunny" } } } } })
print(res4)
res5 = es.search(index="english", body={ "from": 0, "size": 1, "query": { "bool": { "must_not": { "match": { "sentence": "bright" } }, "should": { "match": { "sentence": "sunny" } } } } })
print(res5)

print(es.get(index="english", doc_type="sentences", id=1))
print(es.get(index="english", doc_type="sentences", id=2))
#print(es.get(index="english", doc_type="sentences", id=3))
print(es.search(index="english", body={"from":0, "size":0,"query":{"regexp":{"sentence":".*"}}}))
print(es.search(index="english", body={"from":0, "size":3,"query":{"regexp":{"sentence":".*"}}}))
print(es.search(index="english", body={"from":0, "size":0,"query":{"regexp":{"sentence":"sun.*"}}}))
print(es.search(index="english", body={"from":0, "size":2,"query":{"regexp":{"sentence":".*"}}}))

es.indices.create(index="travel", ignore=400) #index oluşturma
doc6 = {"city":"Bangalore", "country":"India","datetime":"2018,01,01,10,20,00"}
doc7 = {"city":"London", "country":"England","datetime":"2018,01,02,03,12,00"}
doc8 = {"city":"Los Angeles", "country":"USA","datetime":"2018,04,19,21,02,00"}
es.index(index="travel", doc_type="cities", id=1, body=doc6)
es.index(index="travel", doc_type="cities", id=2, body=doc6)
es.index(index="travel", doc_type="cities", id=3, body=doc6)
#es.indices.create(index="travel")

