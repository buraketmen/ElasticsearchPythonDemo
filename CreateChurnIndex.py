from elasticsearch import Elasticsearch
import json
import requests
import pandas as pd
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
mapping = """
    {  
        "mappings":{  
            "properties":{
                "account length":    { "type": "integer"},  
                "area code":  { "type": "integer"}, 
                "number vmail messages":   { "type": "integer"},
                "total day minutes": { "type": "float"},
                "total day calls": { "type": "integer"},
                "total day charge": { "type": "float"},
                "total eve minutes": { "type": "float"},
                "total eve calls": { "type": "integer"},
                "total eve charge": { "type": "float"},
                "total night minutes": { "type": "float"},
                "total night calls": { "type": "integer"},
                "total night charge": { "type": "float"},
                "total intl minutes": { "type": "float"},
                "total intl calls": { "type": "integer"},
                "total intl charge": { "type": "float"},
                "customer service calls": { "type": "integer"},
                "churn": { "type": "boolean"}
            }
        }
    }"""
index_name = "churnintelecoms"
es=Elasticsearch(['url'],http_auth=('user', 'password'))
es.indices.create(index=index_name,ignore=400, body=mapping)

df = pd.read_csv('ChurnTelecoms.csv', sep=',')
numerics = ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']
df.dropna(how='all', inplace=True)
numeric_df = df.select_dtypes(include=numerics)
numeric_columns = numeric_df.columns
for column in numeric_columns:
    df[column].fillna(df[column].mean(), inplace=True)
df.dropna(how="any", inplace=True)
listofdata = df.to_dict('records')
i=1
for item in listofdata:
    es.index(index=index_name, id=i, body=item)
    i+=1