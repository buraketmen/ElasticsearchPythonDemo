from elasticsearch import Elasticsearch
import pandas as pd
import json

def process_hits(df,hits):
    if df is None:
        df = pd.DataFrame.from_dict([item['_source'] for item in hits])
    else:
        df = df.append([item['_source'] for item in hits],ignore_index=True)
    return df

def get_data(es,index,size,body):
    df = pd.DataFrame()
    if not es.indices.exists(index=index):
        print("Index " + index + " not exists")
        exit()

    data = es.search(
        index=index,
        scroll='2m',
        size=size,
        body=body
    )
    sid = data['_scroll_id']
    scroll_size = len(data['hits']['hits'])
    while scroll_size > 0:
        df = process_hits(df,data['hits']['hits'])
        data = es.scroll(scroll_id=sid, scroll='2m')
        sid = data['_scroll_id']
        scroll_size = len(data['hits']['hits'])
    return df