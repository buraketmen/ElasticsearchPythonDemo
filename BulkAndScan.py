from datetime import datetime
from elasticsearch import Elasticsearch
from elasticsearch import helpers
import pandas as pd
import time
mapping = """
    {  
        "mappings":{  
            "properties":{
                "account length":    { "type": "integer"},  
                "area code":  { "type": "integer"}, 
                "number vmail messages":   { "type": "integer"},
                "total day minutes": { "type": "float"},
                "total day calls": { "type": "integer"},
                "total day charge": { "type": "float"},
                "total eve minutes": { "type": "float"},
                "total eve calls": { "type": "integer"},
                "total eve charge": { "type": "float"},
                "total night minutes": { "type": "float"},
                "total night calls": { "type": "integer"},
                "total night charge": { "type": "float"},
                "total intl minutes": { "type": "float"},
                "total intl calls": { "type": "integer"},
                "total intl charge": { "type": "float"},
                "customer service calls": { "type": "integer"},
                "churn": { "type": "boolean"}
            }
        }
    }"""
index_name = "churnintelecoms"
es=Elasticsearch(['url'],http_auth=('user', 'password'))
es.indices.create(index=index_name,ignore=400, body=mapping)
df = pd.read_csv('ChurnTelecoms.csv', sep=',')
listofdata = df.to_dict('records')

# source code: https://stackoverflow.com/questions/20288770/how-to-use-bulk-api-to-store-the-keywords-in-es-by-using-python
actions = [
    {
        "_index": "churnintelecoms",
        "_id":item['phone number'],
        "_source": {
            "state":item['state'],
            'account length':item['account length'],
            'area code':item['area code'],
            'international plan':item['international plan'],
            'voice mail plan':item['voice mail plan'],
            'number vmail messages':item['number vmail messages'],
            'total day minutes':item['total day minutes'],
            'total day calls':item['total day calls'],
            'total day charge':item['total day charge'],
            'total eve minutes':item['total eve minutes'],
            'total eve charge':item['total eve charge'],
            'total night minutes':item['total night minutes'],
            'total night calls':item['total night calls'],
            'total night charge':item['total night charge'],
            'total intl minutes':item['total intl minutes'],
            'total intl calls':item['total intl calls'],
            'total intl charge':item['total intl charge'],
            'customer service calls':item['customer service calls'],
            'churn':item['churn']}
    }
    for item in listofdata
]

st = time.time()
helpers.bulk(es, actions)
end = time.time()
print ("total time", end-st)

#source code: https://stackoverflow.com/questions/47722238/python-elasticsearch-helpers-scan-example
results = helpers.scan(client=es, scroll = '2m', size=500,index=index_name, query={"query": {"match_all": {}}})
for item in results:
    print(item['_source'])



